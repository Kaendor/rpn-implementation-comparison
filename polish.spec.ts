type Operator = (a: number, b: number) => number;

function computeRPN(rpnString: string): number {
  const stack: number[] = [];
  const operations = new Map<string, Operator>();
  operations.set("+", (a, b) => a + b);
  operations.set("-", (a, b) => a - b);

  rpnString.split(" ").forEach((operand) => {
    const parsed = Number(operand);

    if (!isNaN(parsed)) {
      stack.push(parsed);
    } else {
      const lastOne = stack.pop(); // can be undefined
      const lastSecond = stack.pop(); // can be undefined

      const operation = operations.get(operand);
      if (!operation) {
        throw new Error("Unknown Operation");
      }

      stack.push(operation(lastSecond, lastOne));
    }
  });

  return stack.pop(); // can be undefined
}

describe("reversed polish", () => {
  it.each`
    string             | expected
    ${"2 3 +"}         | ${5}
    ${"8 4 3 + - 5 +"} | ${6}
  `("computes a RPN string", ({ string, expected }) => {
    const result = computeRPN(string);
    expect(result).toBe(expected);
  });
});
