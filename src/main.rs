fn main() {
    println!("Hello, world!");
}

fn evaluate_rpn(rpn: &str) -> u32 {
    let mut stack: Vec<u32> = Vec::new();

    for operand in rpn.split(" ") {
        let result = match operand.parse::<u32>() {
            Ok(number) => number,
            Err(_) => perform_operation(stack.pop().unwrap(), stack.pop().unwrap(), operand)
        };

        stack.push(result);
    }

    stack.pop().expect("No number in stack")
}

fn perform_operation(first: u32, second: u32, operator: &str) -> u32 {
    let mut result = second;

    match operator {
        "+" => result += first,
        "-" => result -= first,
        _ => panic!("Unknown operation")
    }

    result
}

#[cfg(test)]
mod test {
    use test_case::test_case;
    use super::*;
    
    #[test_case("1 2 +", 3 ; "with a simple operation")]    
    #[test_case("8 4 3 + - 5 +", 6 ; "with a complex operation")]    
    fn evaluate(input: &str, expected: u32) {
        let result = evaluate_rpn(input);
        assert_eq!(result, expected);
    }

    #[test_case(1, 2, "+", 3 ; "simple addition")]
    #[test_case(2, 2, "+", 4 ; "more simple addition")]
    #[test_case(2, 2, "-", 0 ; "simple substraction")]
    fn operation(first: u32, second: u32, operator: &str, expected: u32) {
        let result = perform_operation(first, second, operator);
        assert_eq!(result, expected);
    }
}